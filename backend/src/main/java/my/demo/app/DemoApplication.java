package my.demo.app;

import my.demo.app.dao.CustomerRepository;
import my.demo.app.dao.EmployeeRepository;
import my.demo.app.model.Customer;
import my.demo.app.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

import static java.util.Arrays.asList;

@SpringBootApplication
@ServletComponentScan
public class DemoApplication implements CommandLineRunner {
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private CustomerRepository customerRepository;



	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// stub data for employees
		Employee employee1 = new Employee("1", "Name", "login", "password");
		Employee employee2 = new Employee("1", "Name 2", "login2", "password");
		employeeRepository.save(asList(employee1, employee2));


		Customer customer1 = new Customer("1", "Company 1");
		Customer customer2 = new Customer("2", "Company 2");
		customerRepository.save(asList(customer1, customer2));

	}
}
