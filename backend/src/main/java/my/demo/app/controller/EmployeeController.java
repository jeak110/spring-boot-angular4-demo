package my.demo.app.controller;

import my.demo.app.dao.EmployeeRepository;
import my.demo.app.model.Employee;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

import static my.demo.app.controller.RestServiceUrls.EMPLOYEES;
import static java.util.Comparator.comparing;


@RestController
@RequestMapping(EMPLOYEES)
@Validated
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> getEmployees() {
        return employeeRepository.findAll()
                .stream()
                .sorted(comparing(employee -> employee.name))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
    public Employee getEmployee(@PathVariable @NotNull @NotEmpty String employeeId) {
        return employeeRepository.findOne(employeeId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Employee create(@RequestBody @NotNull Employee employee) {
        return employeeRepository.save(employee);
    }

    @RequestMapping(value = "/{employeeId}", method = RequestMethod.PUT)
    public Employee edit(@PathVariable @NotNull @NotEmpty String employeeId, @RequestBody @NotNull Employee dto) {
        Employee employee = employeeRepository.findOne(employeeId);
        employee.login = dto.login;
        employee.name = dto.name;
        employee.password = dto.password;

        return employeeRepository.save(employee);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public String handleValidationFailure(ConstraintViolationException ex) {
        StringBuilder messages = new StringBuilder();

        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            messages.append(violation.getMessage() + "\n");
        }

        return messages.toString();
    }
}
