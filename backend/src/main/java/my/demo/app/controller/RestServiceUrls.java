package my.demo.app.controller;

public class RestServiceUrls {
    public static final String CUSTOMERS = "/api/customers";
    public static final String EMPLOYEES = "/api/employees";
}
